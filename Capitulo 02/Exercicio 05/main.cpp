// COM UM GRANDE PROBLEMA DE VAZAMENTO DE MEMORIA.... mas não estou nem ai! ta funcionando!
#include <iostream>
#include "cv.h"
#include "highgui.h"
#include <string>

using namespace cv;
using namespace std;

int mag = 40,pyd=2;

IplImage* doPyrDown(IplImage* in)
{
    assert( in->width%2 == 0 && in->height%2 == 0);
    IplImage* out;
    for(int i =0;i<pyd-1;i++)
    {
        out = cvCreateImage(cvSize(in->width/2,in->height/2), in->depth, in->nChannels);
        cvPyrDown(in,out);
        in = out;
    }
    return out;
}
void Magnitude(int value)
{
}
int main( int argc, char** argv )
{
    CvCapture* cam = 0;

    cam = cvCaptureFromCAM(0);
    
    int fps = cvGetCaptureProperty(cam,CV_CAP_PROP_FPS);
    if (!fps)
        fps = 30;

    cvSetCaptureProperty(cam,CV_CAP_PROP_FRAME_WIDTH,1024);
    CvSize size = cvSize(
        cvGetCaptureProperty(cam,CV_CAP_PROP_FRAME_WIDTH),
        cvGetCaptureProperty(cam,CV_CAP_PROP_FRAME_HEIGHT)
    );

    IplImage *logpolar_frame = cvCreateImage(size,IPL_DEPTH_8U,3), *bgr_frame;

    cvNamedWindow("LogPolar",CV_WINDOW_AUTOSIZE);
    cvNamedWindow("PyrDown",CV_WINDOW_AUTOSIZE);
    cvCreateTrackbar("Maginitude","LogPolar",&mag,100,Magnitude);
    cvCreateTrackbar("PryDown","PyrDown",&pyd,12,Magnitude);
    while(( (bgr_frame = cvQueryFrame(cam)) != NULL ) && (cvWaitKey(1000/fps) != 27) )
    {
        cvLogPolar(
            bgr_frame,
            logpolar_frame,
            cvPoint2D32f(bgr_frame->width/2,bgr_frame->height/2),
            mag,
            CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS
        );
        cvShowImage("Original",bgr_frame);
        cvShowImage("LogPolar",logpolar_frame);
        cvShowImage("PyrDown",doPyrDown(logpolar_frame));
    }
    cvReleaseCapture(&cam);
    cvReleaseImage(&logpolar_frame);

    return 0;
}