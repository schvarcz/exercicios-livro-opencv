// COM UM GRANDE PROBLEMA DE VAZAMENTO DE MEMORIA.... mas não estou nem ai! ta funcionando!
#include <iostream>
#include "cv.h"
#include "highgui.h"
#include <string>

using namespace cv;
using namespace std;

IplImage* doPyrDown(IplImage* in)
{
    assert( in->width%2 == 0 && in->height%2 == 0);
    IplImage* out = cvCreateImage(cvSize(in->width/2,in->height/2), in->depth, in->nChannels);
    cvPyrDown(in,out);
    return out;
}
int main( int argc, char** argv )
{
    CvCapture* cam = 0;
    CvVideoWriter* write = 0;
    string caminho = "d:\\teste.avi";
    cam = cvCaptureFromCAM(0);
    cvSetCaptureProperty(cam,CV_CAP_PROP_FRAME_WIDTH,1024);
    int fps = cvGetCaptureProperty(cam,CV_CAP_PROP_FPS);
    CvSize size = cvSize(
        cvGetCaptureProperty(cam,CV_CAP_PROP_FRAME_WIDTH),
        cvGetCaptureProperty(cam,CV_CAP_PROP_FRAME_HEIGHT)
    );
    if (!fps)
        fps = 30;
    write = cvCreateVideoWriter(
            caminho.c_str(),
            CV_FOURCC_DEFAULT,
            fps,
            cvSize(size.width/2,size.height/2)
    );
    IplImage *logpolar_frame = cvCreateImage(size,IPL_DEPTH_8U,3), *bgr_frame;
    cout << fps << endl;

    while(( (bgr_frame = cvQueryFrame(cam)) != NULL ) && (cvWaitKey(1000/fps) != 27) )
    {
        cvLogPolar(
            bgr_frame,
            logpolar_frame,
            cvPoint2D32f(bgr_frame->width/2,bgr_frame->height/2),
            40,
            CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS
        );
        cvWriteToAVI(write,doPyrDown(logpolar_frame));
    }
    cvReleaseCapture(&cam);
    cvReleaseVideoWriter(&write);
    cvReleaseImage(&logpolar_frame);

    return 0;
}